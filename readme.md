# buzzoole test

## cosa fare?
produrre i seguenti endpoint partendo dalla base di dati fornita

### tt general
endpoint che restituisci la classifica con i top 10 hashtag (le parole identificate da "#") presenti in db

### tt by week
stessa logica dell'endpoint precedente ma con suddivisione settimanale

## come farlo?
stupiscimi!

## utils
nella cartella "dump" trovi l'sql per generate la base dati

se ti va utilizza composer.

## importante!
sentiti libero di trovare la soluzione migliore, e soprattutto, divertiti!

# Soluzione

## Endpoints

- [GET] /top-hashtags
- [GET] /top-hashtags?week=yyyy-mm-dd
- [GET] /top-hashtags?from=yyyy-mm-dd&to=yyyy-mm-dd

## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
