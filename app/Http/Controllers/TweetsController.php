<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Tweet;

class TweetsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param Request $request
     */
    public function topHashTags(Request $request)
    {
        $startDate = null;
        $endDate = null;
        if ($request->has('week'))
        {
            $startDate = Carbon::createFromFormat('Y-m-d', $request->week);
            $startDate->startOfWeek();
            $endDate = Carbon::createFromFormat('Y-m-d', $request->week);
            $endDate->endOfWeek();
        }
        elseif ($request->has('from') && $request->has('to'))
        {
            $startDate = Carbon::createFromFormat('Y-m-d', $request->from);

            $endDate = Carbon::createFromFormat('Y-m-d', $request->to);
        }

        $response = [
            'data' => [
                'dateRange' => [
                    'from' => $startDate ? $startDate->toDateString() : '',
                    'to'   => $endDate ? $endDate->toDateString() : '',
                ],
                'hashtags' =>$this->getTopHashTags($startDate, $endDate)
            ]
        ];

        return response()->json($response);
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return array
     */
    private function getTopHashTags($startDate, $endDate)
    {
        $hashTags = collect();

        if ($startDate && $endDate)
        {
            Tweet::whereBetween('date', [$startDate, $endDate])->chunk(500, function ($tweets) use (&$hashTags)
            {
                $foundHashTags = [];
                foreach ($tweets as $tweet)
                {
                    preg_match_all("/(#\w+)/", $tweet->text, $foundHashTags);

                    foreach ($foundHashTags[0] as $foundHashTag)
                    {
                        if ($hashTags->get($foundHashTag)) {
                            $hashTags->put($foundHashTag, ($hashTags->get($foundHashTag) + 1));
                        } else {
                            $hashTags->put($foundHashTag, 1);
                        }
                    }
                }
            });
        }
        else
        {
            Tweet::chunk(500, function ($tweets) use (&$hashTags)
            {
                $foundHashTags = [];
                foreach ($tweets as $tweet)
                {
                    preg_match_all("/(#\w+)/", $tweet->text, $foundHashTags);

                    foreach ($foundHashTags[0] as $foundHashTag)
                    {
                        if ($hashTags->get($foundHashTag))
                        {
                            $hashTags->put($foundHashTag, ($hashTags->get($foundHashTag) + 1));
                        }
                        else
                        {
                            $hashTags->put($foundHashTag, 1);
                        }
                    }
                }
            });
        }

        $topHashTags = $hashTags->sort()->reverse()->take(10);

        return $topHashTags->all();
    }
}
